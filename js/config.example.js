const config = {
  gitLabDevscolaProjectId: <boardId>,
  deckAsambleasBoard: 2, // Asambleas board in Deck
  labelStackRelation: {
    "Open": 51,
    "Teams": 12,
    "To Do": 4,
    "Doing": 5,
    "Done": 6,
    "Asambleas": 10,
    "Cadaveres": 52,
    "Closed": 53,
  },
}

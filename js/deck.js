const deckAuth = btoa(credentials.deckUser + ":" + credentials.deckPass);

async function migrateData(issues){
  console.log("migrateData");
  let bodies = issues.map( async issue => {
    let title, description, labels, assignees, author, due_date, state, _links;
    ({ title, description, labels, assignees, author, due_date, state, _links } = issue);
    let stackId = null;

    if (state === "closed") {
      stackId = config.labelStackRelation["Closed"];
    } else if (labels.length == 0){
      stackId = config.labelStackRelation["Open"];
    } else {
      stackId = config.labelStackRelation[labels[0]];
    }

    description = description || "\n";

    let notesUrl = _links.notes;

    const notes = await readNotesFromGitLab(notesUrl);

    let filteredNotes = notes.filter(note => note.system === false);
    let formattedNotes = filteredNotes.map(note => {
      return "**" + note.author.name + "** - " + modifyDate(note.updated_at) + "\n\n" +  note.body + "\n\n---\n\n";
    });

    let descriptionComplete = "**Autor**: " + author.name + "\n\n"
    + "**Asignado a**: " + assignees.map(assignee => assignee.name).join(", ") + "\n\n"
    + description + "\n\n---\n\n# Comentarios\n\n---\n\n" + formattedNotes.join("\n\n");

    let body = {
      "title": title,
      "description": descriptionComplete,
      "type": "plain",
      "duedate": modifyDate(due_date),
      "owner": {
        "primaryKey": "devsputnik",
        "uid": "devsputnik",
        "displayname": "Devsputnik",
        "type": 0,
      },
      "stackId": stackId,
    };

    return body;
  });

  const results = await Promise.all(bodies);

  results.forEach((item) => {
    if (item.stackId !== config.labelStackRelation["DoingX"]) {
      createCard(config.deckAsambleasBoard, item.stackId, item);
    }
  });
}

function createCard(board, stack, body){
  let url = `https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/${board}/stacks/${stack}/cards`;
  fetch(url, {
    "method": "POST",
    "headers": {
      "ocs-apirequest": "true",
      "content-type": "application/json",
      "authorization": "Basic " + deckAuth,
    },
    "body": JSON.stringify(body),
  })
  .then(response => {
    response.json();
    console.log(`Tarjeta creada: ${body.title}`);
  })
  .catch(err => {
    console.log(err);
  });

}

function deleteAllCardsInBoard(board){
  getStacksFromBoard(board)
  .then(stacks => {
    let cardIdsToDelete = [];
    stacks.map(stack =>  {
      if(stack.cards){
        stack.cards.map(card => {
          cardIdsToDelete.push([card.stackId, card.id]);
        });
      }
    });
    cardIdsToDelete.forEach((card) => {
      console.log("Deleting: ", board, card[0], card[1]);
      deleteCard(board, card[0], card[1]);
    });

  });
}

function getStacksFromBoard(board){
  console.log("getStacksFromBoard: " + board);
  let url = `https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/${board}/stacks`;
  return fetch(url, {
    "method": "GET",
    "headers": {
      "ocs-apirequest": "true",
      "content-type": "application/json",
      "authorization": "Basic " + deckAuth,
    },
  })
  .then(response => {
    return response.json();
  })
  .then(stacks => {
    return stacks;
  })
  .catch(err => {
    console.log(err);
  });

}

function deleteCard(board, stack, card){
  console.log("deleteCard: ", board, stack, card);
  let url = `https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/${board}/stacks/${stack}/cards/${card}`;
  fetch(url, {
    "method": "DELETE",
    "headers": {
      "ocs-apirequest": "true",
      "content-type": "application/json",
      "authorization": "Basic " + deckAuth,
    },
  })
  .then(response => {
    console.log(response);
  })
  .catch(err => {
    console.log(err);
  });

}

function onLoad() {
  addEventsListeners();
}

function addEventsListeners() {
  document.getElementById("migrate-all").addEventListener("click", migrateGitLabToDeck);
  document.getElementById("delete-all").addEventListener("click", deleteAllAsambleaCards);
}


async function migrateGitLabToDeck() {
  console.log("migrateGitLabToDeck");
  await readIssuesFromGitLab(config.gitLabDevscolaProjectId);
}

function deleteAllAsambleaCards() {
  deleteAllCardsInBoard(config.deckAsambleasBoard);
}

function modifyDate(dateString) {
  let modifiedDate = null;
  if (dateString) {
    const MS_PER_MINUTE = 60000;
    const MINUTES_PER_HOUR = 60;
    const HOURS_TO_SUBSTRACT = 0;

    modifiedDate = new Date(new Date(dateString) - HOURS_TO_SUBSTRACT * MINUTES_PER_HOUR * MS_PER_MINUTE);
  }
  return modifiedDate;
}


function readIssuesFromGitLab(projectId){

  fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues?per_page=100`, {
    "method": "GET",
    "headers": {  }
  })
  .then(response => {
    // console.log(response);
    return response.json();
  })
  .then( async data => {
    // console.log(data);
    await migrateData(data);
  })
  .catch(err => {
    console.log(err);
  });

}

function readNotesFromGitLab(url){
  return fetch(url, {
    "method": "GET",
    "headers": {
      "authorization": `Bearer ${credentials.gitlabToken}`
    }
  })
    .then(response => {
      return response.json();
    })
    .then( data => {
      return data;
    })
    .catch(err => {
      console.log(err);
    });
}
